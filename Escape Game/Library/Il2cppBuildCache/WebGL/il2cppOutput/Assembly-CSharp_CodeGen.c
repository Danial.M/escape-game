﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AnimationStateController::Start()
extern void AnimationStateController_Start_m9909F040CA02623E569AE44DD347B3C46FE89850 (void);
// 0x00000002 System.Void AnimationStateController::Update()
extern void AnimationStateController_Update_m3B63902680B71A2D3BC220BEACDA18568537F5B3 (void);
// 0x00000003 System.Void AnimationStateController::.ctor()
extern void AnimationStateController__ctor_m5EF50D0AE710D0AF90552EB091B22E61CAEE5CB4 (void);
// 0x00000004 System.Void CameraScript::Update()
extern void CameraScript_Update_mFD3343EC9B5F59B8D44E7151FA49C7CE88B1936A (void);
// 0x00000005 System.Void CameraScript::.ctor()
extern void CameraScript__ctor_m2F555CA91AB185743DD59C5C093A7F6EE339B863 (void);
// 0x00000006 System.Void Credits::Quit()
extern void Credits_Quit_mA7C412B1FB2D281D2704053CEA4C7677B197012D (void);
// 0x00000007 System.Void Credits::GoToMenu()
extern void Credits_GoToMenu_m1DC71D38FE32581F3510D82BC6F2A09DE457812C (void);
// 0x00000008 System.Void Credits::.ctor()
extern void Credits__ctor_m303F0FF4F6858911FFF3C8A6F1BDCA780D273A35 (void);
// 0x00000009 System.Void EndTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void EndTrigger_OnTriggerEnter_mCD200852C7B6F7E1D30245FAD7F21FFDB3C30C3B (void);
// 0x0000000A System.Void EndTrigger::.ctor()
extern void EndTrigger__ctor_m396FC2C4F7B51D28A3AD9DBE011F3B509CD2AE3F (void);
// 0x0000000B System.Void EnemyFollow::Update()
extern void EnemyFollow_Update_m93E1941810AE9F9FCFB46BEC614596794EBC50A7 (void);
// 0x0000000C System.Void EnemyFollow::.ctor()
extern void EnemyFollow__ctor_mE1C38FA382CDB4CC8F3D48D70A49189FB13592CC (void);
// 0x0000000D System.Void GameManage::CompleteMaze()
extern void GameManage_CompleteMaze_mDFDAFD4F55F7FB6373C8C1E130F7DC8A72BBC551 (void);
// 0x0000000E System.Void GameManage::EndGame()
extern void GameManage_EndGame_m420E730CEB6223944F03CD7EE784AC6950A0EFAC (void);
// 0x0000000F System.Void GameManage::.ctor()
extern void GameManage__ctor_mC37280FAA0CA340D94873873B847A57222D614E8 (void);
// 0x00000010 System.Void GameOver::Restart()
extern void GameOver_Restart_mE653EE462371F3543C2B823D158C2615B2CD8FD9 (void);
// 0x00000011 System.Void GameOver::GoToMenu()
extern void GameOver_GoToMenu_m37C4B477295BF55D20C145392E830FD830A54397 (void);
// 0x00000012 System.Void GameOver::.ctor()
extern void GameOver__ctor_mA5E40A40961F188E4CE253D481E8AED893A224E9 (void);
// 0x00000013 System.Void MazeComplete::LoadNextLevel()
extern void MazeComplete_LoadNextLevel_m5A6C5AC71D819654B8E84B8502E19F8150ABEB68 (void);
// 0x00000014 System.Void MazeComplete::.ctor()
extern void MazeComplete__ctor_mC22378BF9F01972359F6EA9EE0EEB5CD0EE6BB39 (void);
// 0x00000015 System.Void Menu::StartGame()
extern void Menu_StartGame_m6B2E9CA9AE74433EA78DE12BADAAFA800B15DFC0 (void);
// 0x00000016 System.Void Menu::QuitGame()
extern void Menu_QuitGame_mC0A307F2681D0D684232694A10404FAAA8269A7A (void);
// 0x00000017 System.Void Menu::.ctor()
extern void Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134 (void);
// 0x00000018 System.Void MouseLook::Start()
extern void MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541 (void);
// 0x00000019 System.Void MouseLook::Update()
extern void MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5 (void);
// 0x0000001A System.Void MouseLook::.ctor()
extern void MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B (void);
// 0x0000001B System.Void PlayerCollision::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerCollision_OnTriggerEnter_m9920940DF1D81A113B963E84FD4430BB8ABE2F76 (void);
// 0x0000001C System.Void PlayerCollision::.ctor()
extern void PlayerCollision__ctor_mB6CC65E3E3B37769F12E0283C5B014269A9065E8 (void);
// 0x0000001D System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x0000001E System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
static Il2CppMethodPointer s_methodPointers[30] = 
{
	AnimationStateController_Start_m9909F040CA02623E569AE44DD347B3C46FE89850,
	AnimationStateController_Update_m3B63902680B71A2D3BC220BEACDA18568537F5B3,
	AnimationStateController__ctor_m5EF50D0AE710D0AF90552EB091B22E61CAEE5CB4,
	CameraScript_Update_mFD3343EC9B5F59B8D44E7151FA49C7CE88B1936A,
	CameraScript__ctor_m2F555CA91AB185743DD59C5C093A7F6EE339B863,
	Credits_Quit_mA7C412B1FB2D281D2704053CEA4C7677B197012D,
	Credits_GoToMenu_m1DC71D38FE32581F3510D82BC6F2A09DE457812C,
	Credits__ctor_m303F0FF4F6858911FFF3C8A6F1BDCA780D273A35,
	EndTrigger_OnTriggerEnter_mCD200852C7B6F7E1D30245FAD7F21FFDB3C30C3B,
	EndTrigger__ctor_m396FC2C4F7B51D28A3AD9DBE011F3B509CD2AE3F,
	EnemyFollow_Update_m93E1941810AE9F9FCFB46BEC614596794EBC50A7,
	EnemyFollow__ctor_mE1C38FA382CDB4CC8F3D48D70A49189FB13592CC,
	GameManage_CompleteMaze_mDFDAFD4F55F7FB6373C8C1E130F7DC8A72BBC551,
	GameManage_EndGame_m420E730CEB6223944F03CD7EE784AC6950A0EFAC,
	GameManage__ctor_mC37280FAA0CA340D94873873B847A57222D614E8,
	GameOver_Restart_mE653EE462371F3543C2B823D158C2615B2CD8FD9,
	GameOver_GoToMenu_m37C4B477295BF55D20C145392E830FD830A54397,
	GameOver__ctor_mA5E40A40961F188E4CE253D481E8AED893A224E9,
	MazeComplete_LoadNextLevel_m5A6C5AC71D819654B8E84B8502E19F8150ABEB68,
	MazeComplete__ctor_mC22378BF9F01972359F6EA9EE0EEB5CD0EE6BB39,
	Menu_StartGame_m6B2E9CA9AE74433EA78DE12BADAAFA800B15DFC0,
	Menu_QuitGame_mC0A307F2681D0D684232694A10404FAAA8269A7A,
	Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134,
	MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541,
	MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5,
	MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B,
	PlayerCollision_OnTriggerEnter_m9920940DF1D81A113B963E84FD4430BB8ABE2F76,
	PlayerCollision__ctor_mB6CC65E3E3B37769F12E0283C5B014269A9065E8,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
};
static const int32_t s_InvokerIndices[30] = 
{
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1017,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1194,
	1017,
	1194,
	1194,
	1194,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	30,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
