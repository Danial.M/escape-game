using UnityEngine;
using UnityEngine.SceneManagement;


public class Credits : MonoBehaviour
{

    public void Quit()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");

    }
}
