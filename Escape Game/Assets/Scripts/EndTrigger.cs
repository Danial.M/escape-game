using UnityEngine;

public class EndTrigger : MonoBehaviour
{

    public GameManage gameManager;

    void OnTriggerEnter (Collider hit)
    {
        if (hit.name == "First Person Player")
        {
            gameManager.CompleteMaze();
        } 
    }

}
