using UnityEngine;
using UnityEngine.SceneManagement;

public class MazeComplete : MonoBehaviour
{

    public void LoadNextLevel ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
