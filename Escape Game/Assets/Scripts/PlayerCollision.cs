using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement movement;     // Reference to PlayerMovement script

    private void OnTriggerEnter(Collider hit)
    {
        // Check if player collided with tag "Enemy"
        if (hit.tag == "Enemy")
        {
            Debug.Log("We hit an enemy");
            movement.enabled = false; // Disable players movement
            FindObjectOfType<GameManage>().EndGame();

        }
    }
}
