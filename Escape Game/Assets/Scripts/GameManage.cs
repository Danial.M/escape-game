using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManage : MonoBehaviour
{
    bool gameHasEnded = false;

    public GameObject completeMazeUI;
    public GameObject GameOverUI;


    public void CompleteMaze()
    {
        // Game has ended set to true, so EndGame() function cannot be called
        gameHasEnded = true;    
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        completeMazeUI.SetActive(true);
        Debug.Log("MAZE COMPLETE");

    }

    public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            GameOverUI.SetActive(true);
            Debug.Log("GAME OVER");
        }
        
    }


}
